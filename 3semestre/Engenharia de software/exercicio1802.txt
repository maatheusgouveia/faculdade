-------------------------------------- Exercício 1 -----------------------------------------------
Requisitos funcionais
- Cadastro de usuários
- Logs de entrada e saída de clientes dos quartos
- usuários com diferentes permissões (Adm, recepcionista, gerente...)
- Logs de pedidos e serviços prestados
- Consulta de vagas
- Consulta de características e preços dos quartos
- Fazer, editar e cancelar reserva por meio da aplicação web
- Ver fotos do quarto
- Consultar vagas disponíveis em outros hotéis da rede
-
-
-
-
-
-

Requisitos não funcionais
- Linguagem usada no desenvolvimento será o PHP no back-end
- Linguagem front-end será Javascript utilisando React JS
- Banco de dados oracle
- Servidor será um AWS (Amazon Web Services)
- Comunicação via API Rest com JSON
- 


-------------------------------------- Exercício 2 -----------------------------------------------

Requisitos funcionais
- O sistema tem cadastro de usuários
- sistema de permissões
- Controle de estoque
- 

requisitos não funcionais
- Banco de dados firebird 2.5
- Linguagem de programação usada Delphi 10
- 


