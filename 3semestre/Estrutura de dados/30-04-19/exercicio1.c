#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define TAMANHO 2000

int main () {

    int numeros[TAMANHO];

    printf("*** Carregando o vetor ***\n");

    srand(time(NULL));

    for (int i = 0; i < TAMANHO; i++)
    {
        numeros[i] = rand() % 1000000;
        printf("%d\n", numeros[i]);
    }

    printf("\n*** Vetor carregado ***\n");    

    return 0;
}
    