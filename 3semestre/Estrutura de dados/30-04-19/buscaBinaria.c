#include <stdio.h>
#include <stdlib.h>

int buscaBinaria(int conjunto[], int chaveDeBusca, int tamanho);

int main () {
    int vetor[1000];

    for (int i = 0; i < 1000; i++) {
        vetor[i] = i + 1;
    }

    int valorBusca = -1;

    do {
        printf("Insira um número para ser buscado: ");
        scanf("%d", &valorBusca);

        if (valorBusca < 0 || valorBusca > 1000) {
            system("clear");
            printf("Digite um valor entre 1 e 1000\n");
            system("clear");
        }
    } while (valorBusca < 0 && valorBusca > 1000);

    int posicao = buscaBinaria(vetor, valorBusca, 1000);

    printf("O índice do valor: %d é: %d\n", valorBusca, posicao);

    return 0;
}

int buscaBinaria(int conjunto[], int chaveDeBusca, int tamanho) {
    int inferior = 0;
    int superior = tamanho - 1;
    int meio;

    while (inferior <= superior) {
        meio = (inferior + superior) / 2;

        if (chaveDeBusca == conjunto[meio]) {
            return meio;
        } else if (chaveDeBusca < conjunto[meio]) {
            superior = meio - 1;
        } else {
            inferior = meio + 1;
        }        
    }
    return -1;
}