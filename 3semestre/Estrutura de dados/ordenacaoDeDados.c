#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define TAMANHO 2000000

//ordenação Bublesort

int main() {

	int numeros[TAMANHO];
	
	printf("*** Carregando o vetor ***\n");
	
	srand(time(NULL));

	for(int i = 0; i < TAMANHO; i++) {
		numeros[i] = rand() % 500;
	}

	printf("\n*** Vetor carregado ***\n");

	for(int i = 0; i < TAMANHO; i++) {	
		printf("%d", numeros[i]);

		if(i + 1 == TAMANHO) {
			printf("\n");
		} else {
			printf(", ");
		}
	}

	printf("\n*** Ordenando valores ***\n");
	
	//bubblesort canonico
	//for(int i = 1; i < TAMANHO; i++) {
		//for(int j = 0; j < TAMANHO - 1; j++) {
			//if(numeros[j] > numeros [j + 1]) {
				//int aux = numeros[j];
				//numeros[j] = numeros[j + 1];
				//numeros[j + 1] = aux;
			//}
		//}
	//}
	

	//sort Prof Gustavo edition
	for(int i = 0; i < TAMANHO; i++) {
		if(i + 1 <= TAMANHO) {
			if(numeros[i] > numeros[i + 1]) {
				int aux = numeros[i + 1];
				numeros[i + 1] = numeros[i];
				numeros[i] = aux;
				i = -1;
			}
		}
	}

	printf("\n*** Vetor ordenado ***\n");

	for(int i = 0; i < TAMANHO; i++) {	
		printf("%d", numeros[i]);

		if(i + 1 == TAMANHO) {
			printf("\n");
		} else {
			printf(", ");
		}
	}

	return 0;
}
