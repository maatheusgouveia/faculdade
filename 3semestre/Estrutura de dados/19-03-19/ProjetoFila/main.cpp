#include <stdio.h>
#include <stdlib.h>

struct Elemento {
	int num;
	struct Elemento *proximo;
};

typedef struct Elemento elemento;

int tamanho;
//escopos de função
void inicial(elemento *FILA);
int menu();
void processaMenu(elemento *FILA, int opcao);
void insereElemento(elemento *FILA);
void exibir(elemento *FILA);

int main() {

	elemento *FILA = (elemento *) malloc(sizeof(elemento));
	inicial(FILA);
	int escolha = 0;
	
	do{
		escolha = menu();
	}while(escolha > 0);
	
	return 0;	
}

void inicial(elemento *FILA) {
	FILA->proximo = NULL;
	tamanho = 0;
}

int menu() {
	int opcao;
	printf("************************************************************************\n");
	printf("*******************          Controle de fila          *****************\n");
	printf("************************************************************************\n");
	printf("************************************************************************\n");
	printf("****                                                                ****\n");
	printf("* 1. Zerar fila                                                        *\n");
	printf("* 2. Exibir elementos                                                  *\n");
	printf("* 3. Adicionar Elemento                                                *\n");
	printf("* 4. Remover Elemento                                                  *\n");
	printf("* 5. Sair                                                              *\n");
	printf("****                                                                ****\n");
	printf("************************************************************************\n");
	printf("************************************************************************\n");
	printf("Opção: ");
	scanf("%d", &opcao);
	return opcao;
}

void processaMenu(elemento *FILA, int opcao) {
	switch(opcao) {
		case 3:
			insereElemento(FILA);
			break;
	}
}

void insereElemento(elemento *FILA) {
	elemento *novoElemento = (elemento *) malloc(sizeof(elemento));
	printf("Insira o valor do elemento:\n");
	scanf("%d", &novoElemento.num);
	elemento *antigoElemento = FILA->proximo;

	FILA->proximo = novoElemento;
	novoElemento->proximo = antigoElemento;
}

void exibirElemento(elemento *FILA) {
	system("clear");
	if(FILA->proximo == NULL) {
		printf("A fila está vazia\n");
	} else {
		
	}
}













