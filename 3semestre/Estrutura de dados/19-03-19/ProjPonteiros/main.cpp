#include <stdio.h>
#include<stdlib.h>

int main() {
	int i = 10;
	int j = i;
	int *ponteiro;
	
	ponteiro = &i;
	
	j -= 2;
	i -= 3;
	
	printf("O tamanho de uma variável do tipo int em bytes é %d e em bits é %d  \n", sizeof(i), sizeof(i) * 8);
	
	printf("O valor da variável i é: %d \n", &i);
	printf("O endereço da variável i é: %d \n", i);
	
	printf("O valor da variável j é: %d \n", &j);
	printf("O endereço da variável j é: %d \n", j);
	
	printf("O valor do ponteiro é %d \n", *ponteiro);
	
	return 0;
}
