#include <stdio.h>
#include <stdlib.h>

long fibonacci(long n);

int main() {
    long penultimo = 1;
    long ultimo = 0;
    long aux = 0;
    
    for (int i = 0; i <= 10000; i++) {
        aux = penultimo + ultimo;
        printf("%lu ", aux);
        penultimo = ultimo;
        ultimo = aux;
    }

    printf("\n");

    return 0;
}

long fibonacci(long n) {
    if (n == 0 || n == 1) {
        return n;
    } else {
        return fibonacci(n - 1) + fibonacci(n - 2);
    }
}