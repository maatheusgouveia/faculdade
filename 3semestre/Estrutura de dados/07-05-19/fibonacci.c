#include <stdio.h>
#include <stdlib.h>

long fibonacci(long n);

int main() {
    for (int i = 0; i < 100; i++) {
        printf("%lu ", fibonacci(i + 1));
    }
    printf("\n");

    return 0;
}

long fibonacci(long n) {
    if (n == 0 || n == 1) {
        return n;
    } else {
        return fibonacci(n - 1) + fibonacci(n - 2);
    }
}