#include <stdio.h>
#include <stdlib.h>

int f(int v[], int n);

int main () {
    int x[] = { 2, -4, 7, 0, -1, 4 };
    printf("%d \n", f(x, 6));

    return 0;
}

int f(int v[], int n) {
    if (n == 0) {
        return 0;
    } else {
        int s = f(v, n - 1);
        if (v[n - 1] > 0) {
            s = s + v[n - 1];
        }
        return s;
    }
}
