/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;
import java.sql.*;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author matheusgouveia
 */
public class LoginDAO {
    
    Connection con;
    String sql;
    PreparedStatement pst;
    ResultSet rs;
    
    public void consultarUsuarioLogin(JTextField loginUsu, JPasswordField senhaUsu, JFrame jfLogin, JFrame jfPrincipal) {
        try {
            con = Conexao.conectar();
            sql = "SELECT * FROM Usuario WHERE usu_login = ? AND usu_senha = md5(?)";
            pst =  con.prepareStatement(sql);
            pst.setString(1, loginUsu.getText());
            pst.setString(2, senhaUsu.getText());
            rs = pst.executeQuery();
            
            if (rs.next()) {
                JOptionPane.showMessageDialog(jfLogin, "Acesso perimitido");
                jfPrincipal.setVisible(true);
                jfLogin.setVisible(false);
            } else {
                JOptionPane.showMessageDialog(jfLogin, "Acesso acesso negado");
                loginUsu.setText("");
                senhaUsu.setText("");
                loginUsu.requestFocus();
            }
            
            Conexao.desconectar();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(jfLogin, "Erro ao acessar: " + e);
        }
    }
    
}
