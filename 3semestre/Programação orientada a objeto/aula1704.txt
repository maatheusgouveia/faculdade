********************************modificadores em classes***********************************

classe abstract - Não pode ser instanciada (Algum método não foi implementado), serve para ser derivada;

classe final - Não pode ser derivada;

método final- Não pode ser sobrescrito;

atributo final - é uma constante;

método static - chamado sem a necessidade de uma instância;

atributo static - Existe apenas uma única "cópia" para todos os objetos da classe;


********************************Passos sugeridos para criação de uma classe gráfica***********************************

1 - importar os pacotes java.awt
2 - declarar a classe cp,p derivada de JFrame
3 - declarar os componentes como atributos da classe
4 - no construtor:
4.1 - instanciar os componentes
4.2
