#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define TAMANHO 15

//ordenação Bublesort

int main() {

	int numeros[TAMANHO] = {124, 294, 508, 330, 102, 247, 77, 39, 467, 182, 19, 3, 668, 180, 27};	

	for(int i = 0; i < TAMANHO; i++) {	
		printf("%d", numeros[i]);

		if(i + 1 == TAMANHO) {
			printf("\n");
		} else {
			printf(", ");
		}
	}

	printf("\n*** Ordenando valores ***\n");
	
	//bubblesort canonico
	for(int i = 1; i < TAMANHO; i++) {
		for(int j = 0; j < TAMANHO - 1; j++) {
			if(numeros[j] > numeros [j + 1]) {
				int aux = numeros[j];
				numeros[j] = numeros[j + 1];
				numeros[j + 1] = aux;
			}
		}
	}	

	printf("\n*** Vetor em ordem crescente ***\n");

	for(int i = 0; i < TAMANHO; i++) {	
		printf("%d", numeros[i]);

		if(i + 1 == TAMANHO) {
			printf("\n");
		} else {
			printf(", ");
		}
	}

	return 0;
}
