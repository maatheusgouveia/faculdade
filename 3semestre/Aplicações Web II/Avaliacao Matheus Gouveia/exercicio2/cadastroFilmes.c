#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct filmes{
    long id_filme;
    char nome_filme[100];
    char genero_filme [50];
    int ano_lancamento_filme;
    int duracao_filme;
}filmesTipos;

FILE *arquivo;

void carregaMenu();
void opcaoMenu();
void opcaoInvalida();
void cadastraFilmes();

void preparaArquivo();
void erroArquivo();
void salvarFilme(filmesTipos filme);
void lerArquivo();
void excluir();

int main()
{
    preparaArquivo();
    carregaMenu();

    return 0;
}

void carregaMenu(){
    system("clear");
    printf("*************************************\n");
    printf("*                                   *\n");
    printf("*                                   *\n");
    printf("*         CADASTRO DE FILMES        *\n");
    printf("*                                   *\n");
    printf("*                                   *\n");
    printf("*************************************\n");
    printf("*                                   *\n");
    printf("* 1. - Cadastrar filme              *\n");
    printf("*                                   *\n");
    printf("* 2. - Listar filmes                *\n");
    printf("*                                   *\n");
    printf("* 3. - Excluir filme                *\n");
    printf("*                                   *\n");
    printf("* 4. - Editar filme                 *\n");
    printf("*                                   *\n");
    printf("* 5. - Sair                         *\n");
    printf("*                                   *\n");
    printf("*************************************\n");
    opcaoMenu();
}

void opcaoMenu(){
    char opcao = 0;
    printf("*************************************\n");
    printf("\n\nDigite a Opcao Desejada: ");
    scanf("%d",&opcao);

    switch(opcao){
        case 1:
            cadastraFilmes();
        break;

        case 2:
            lerArquivo();
        break;

        case 3:
            excluir();
        break;

        case 5:
            return;
        break;

        default:
            opcaoInvalida();
    }
}

void opcaoInvalida(){
    system("clear");
    printf("Opcao Invalida!!\n\n");
//    system("PAUSE");
    carregaMenu();
}

void cadastraFilmes(){
        system("clear");

        filmesTipos listaFilmes;

        printf("Insira o ID do filme\n");
        do{
            scanf("%d",&listaFilmes.id_filme);
        }while(listaFilmes.id_filme <= 0);

        printf("Insira o nome do filme:\n");
        do{
            gets(listaFilmes.nome_filme);
        }while(listaFilmes.nome_filme[0] == 0);

        printf("Insira o genero do filme:\n");
        do{
            gets(listaFilmes.genero_filme);
        }while(listaFilmes.genero_filme[0] == 0);

        printf("Insira o ano de lancamento:\n");
        do{
            scanf("%d",&listaFilmes.ano_lancamento_filme);
        }while(listaFilmes.ano_lancamento_filme == 0);

        printf("Insira a duracao do filme:\n");
        do{
            gets(listaFilmes.duracao_filme);
        }while(listaFilmes.duracao_filme[0] == 0);

        salvarFilme(listaFilmes);
        carregaMenu();

}

void preparaArquivo(){
    arquivo = fopen("dados","ab+");
}

void erroArquivo(){
    system("clear");
    printf("Falha ao carregar arquivo de dados!!!!\n\n\n");

    //system("PAUSE");
    carregaMenu();
}

void salvarFilme(filmesTipos filme){
    if(arquivo == NULL){
        erroArquivo();
        return;
    }

    fwrite(&filme,sizeof(filme),1,arquivo);
    fflush(arquivo);
}

void lerArquivo(){
    preparaArquivo();
    system("clear");
    filmesTipos filme;
    while(TRUE){
        fread(&filme,sizeof(filmesTipos),1,arquivo);

        if(feof(arquivo)){
            break;
        }

        printf("*************************\n");
        printf("id Filme: %d\n",filme.id_filme);
        printf("Nome do Filme: %s\n",filme.nome_filme);
        printf("Genero do filme: %s\n",filme.genero_filme);
        printf("ano lancamento: %d\n",filme.ano_lancamento_filme);
        printf("Duracao: %d\n",filme.duracao_filme);
    }
    printf("*************************\n");
//    system("PAUSE");
    carregaMenu();

}

void excluir(){
    int idSel = 0;
    char opcao = 0;
    preparaArquivo();
    //system("CLS");
    filmesTipos filme;
    /* Exibe todos os dados em tela, para uma melhor usabilidade */
    while(TRUE){
        fread(&filme,sizeof(filmesTipos),1,arquivo);

        if(feof(arquivo)){
            break;
        }

        printf("*************************\n");
        printf("id filme: %d\n",filme.id_filme);
        printf("Nome do filme: %s\n",filme.nome_filme);
        printf("Genero do filme: %s\n",filme.genero_filme);
        printf("Ano lancamento: %s\n",filme.ano_lancamento_filme);
        printf("Duracao: %d\n",filme.duracao_filme);
    }
    printf("*************************\n");

    printf("Informe o id do filme que deseja excluir, ou digite -1 para cancelar: \n");
    scanf("%d",&idSel);

    if(idSel == -1){
        carregaMenu();
        return;
    }

    /*cria arquivo de despejo temporario*/
    FILE *arquivoTemp = fopen("dados~","w");

    /*carrega o arquivo com todos os dados*/
    arquivo = fopen("dados","r");

    /*Lê todo o arquivo até encontrar o registro que se deseja excluir*/
    while(TRUE){
        fread(&filme,sizeof(filmesTipos),1,arquivo);

        if(feof(arquivo)){
            break;
        }

        if(filme.id_filme == idSel){
            system("clear");
            printf("Tem certeza que deseja excluir o registro:\n");
            printf("*************************\n");
            printf("id filme: %l\n",filme.id_filme);
            printf("Nome do filme: %s\n",filme.nome_filme);
            printf("Genero do filme: %s\n",filme.genero_filme);
            printf("ano de lancamento: %d\n",filme.ano_lancamento_filme);
            printf("Duracao: %d\n",filme.duracao_filme);
            printf("*************************\n");
            printf("[S/N]:\n");
            fflush(stdin);
            scanf("%c",&opcao);

            if(opcao == 'N'|| opcao == 'n'){
                fwrite(&filme,sizeof(filmesTipos),1,arquivoTemp);
            }
        }else{
            fwrite(&filme,sizeof(filmesTipos),1,arquivoTemp);
        }
    }

    /*forca a escrita dos dados e fecha o arquivo*/
    fflush(arquivoTemp);
    fclose(arquivoTemp);
    fclose(arquivo);

    /*Abre o aruivo de dados para persistir os dados sem o registro excluído*/
    arquivo = fopen("dados","w");
    arquivoTemp = fopen("dados~","r");

    /* Lê os dados do arquivo temporário e grava no arquivo final */
     while(TRUE){
        fread(&filme,sizeof(filmesTipos),1,arquivoTemp);
        if(feof(arquivoTemp)){
            break;
        }
        fwrite(&filme,sizeof(filmesTipos),1,arquivo);
    }

    fflush(arquivoTemp);
    fflush(arquivo);
    fclose(arquivoTemp);
    fclose(arquivo);
    system("clear");
    printf("Filme excluído com Sucesso!!!\n");

//    system("PAUSE");
    carregaMenu();
}




